using System.Collections.Generic;

namespace Blog.Utils
{
    public class DescendingEqualityComparer : IEqualityComparer<decimal>
    {
        public bool Equals(decimal x, decimal y)
        {
            return x == y;
        }
    
        public int GetHashCode(decimal obj)
        {
            return obj.GetHashCode();
        }
    }
}