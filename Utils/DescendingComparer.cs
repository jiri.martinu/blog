using System.Collections.Generic;

namespace Blog.Utils
{
    public class DescendingComparer<T> : IComparer<T>
    {
        public int Compare(T x, T y)
        {
            return Comparer<T>.Default.Compare(y, x);
        }
    }
}