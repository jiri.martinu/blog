using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Blog.Models.Settings;

namespace Blog.Models.Seeding
{
    public static class CreateRoles
    {
        public static async Task DoCreateRoles(IServiceProvider serviceProvider, IConfiguration Configuration)
        {
            //adding customs roles
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<User>>();
            var adminUserSettings = serviceProvider.GetRequiredService<AdminUserSettings>();

            string[] roleNames = { "Admin", "Manager", "Member" };
            IdentityResult roleResult;

            foreach (var roleName in roleNames) {
                // creating the roles and seeding them to the database
                var roleExist = await roleManager.RoleExistsAsync(roleName);

                if (!roleExist) {
                    roleResult = await roleManager.CreateAsync(new IdentityRole(roleName));
                }
            }

            // creating a super user who could maintain the web app
            var poweruser = new User
            {
                UserName = adminUserSettings.Email,
                Email = adminUserSettings.Email
            };

            string userPassword = adminUserSettings.Password;
            var user = await userManager.FindByEmailAsync(adminUserSettings.Email);

            if (user == null) {
                var createPowerUser = await userManager.CreateAsync(poweruser, userPassword);

                if (createPowerUser.Succeeded) {
                    // here we assign the new user the "Admin" role 
                    await userManager.AddToRoleAsync(poweruser, "Admin");
                }
            }
        }
    }
} 