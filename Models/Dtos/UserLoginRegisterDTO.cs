namespace Blog.Models.Dtos
{
    public class UserLoginRegisterDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}