namespace Blog.Models.Dtos
{
    public class UserFrontDTO
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}