namespace Blog.Models.Settings
{
    public class AdminUserSettings
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }
}