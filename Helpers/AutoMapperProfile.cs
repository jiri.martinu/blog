using AutoMapper;
using Blog.Models;
using Blog.Models.Dtos;

namespace Blog.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserLoginRegisterDTO>();
            CreateMap<UserLoginRegisterDTO, User>();
            CreateMap<User, UserFrontDTO>();
            CreateMap<UserFrontDTO, User>();
        }
    }
}