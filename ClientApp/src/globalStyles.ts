import { createGlobalStyle } from 'styled-components';
import { Theme } from './utils/Theme';

export const GlobalStyles = createGlobalStyle`
  body {
    padding-top: 70px;
    padding-bottom: 30px;
    margin: 0;
    font-size: 1.6rem;
    font-weight: 300;
    font-family: "Roboto", "Helvetica Neue", Helvetica, Arial, sans-serif;
    line-height: 1.5;
    color: ${Theme.colors.fonts.default};
    text-align: left;
    background-color: ${Theme.colors.backgrounds.background};
  }

  h1, h2, h3, h4, th, label {
    color: ${Theme.colors.fonts.titles};
    font-weight: 300;
  }

  h1 {
    font-size: 4rem;
    line-height: 1.2;
  }

  p {
    font-weight: 300;
  }

  a {
    color: ${Theme.colors.fonts.links};
  }

  input {
    background-color: ${Theme.colors.backgrounds.input};
    border: 1px solid ${Theme.colors.backgrounds.input};
    border-radius: ${Theme.borders.radius};
    padding: 0.25rem;
  }

  b, strong {
    font-weight: 400;
    color: ${Theme.colors.fonts.titles};
  }

  table {
    border-collapse: separate;
    border-spacing: 0;

    tr {
      th, td {
        border-right: 1px solid ${Theme.colors.borders.table};
        border-bottom: 1px solid ${Theme.colors.borders.table} !important;
        padding: 5px;

        &:first-child {
          border-left: 1px solid ${Theme.colors.borders.table};
        }
      }

      th {
        border-top: 1px solid ${Theme.colors.borders.table} !important;
        text-align: left;
      }

      td {
        border-top: none !important;
      }

      &:first-child {
        th {
          &:first-child {
            border-top-left-radius: ${Theme.borders.radius};
          }
          &:last-child {
            border-top-right-radius: ${Theme.borders.radius};
          }
        }

        td {
          &:first-child {
            border-bottom-left-radius: ${Theme.borders.radius};
          }
          &:last-child {
            border-bottom-right-radius: ${Theme.borders.radius};
          }
        }
      }
    }
  }
`