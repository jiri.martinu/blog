import * as Counter from './Counter';
import * as WeatherForecasts from './WeatherForecasts';
import { IUsersState } from './states/UsersState';
import { AlertReducer } from './reducers/AlertReducer';
import { AuthenticationReducer } from './reducers/AuthenticationReducer';
import { IAlertState } from './states/AlertState';
import { reducer as formReducer } from 'redux-form'
import { IAuthenticationState } from './states/AuthenticationState';

// The top-level state object
export interface IApplicationState {
    counter: Counter.ICounterState;
    weatherForecasts: WeatherForecasts.IWeatherForecastsState;
    alert: IAlertState;
    users: IUsersState;
    authentication: IAuthenticationState;
}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {
    counter: Counter.reducer,
    weatherForecasts: WeatherForecasts.reducer,
    alert: AlertReducer,
    // users: UsersReducer,
    authentication: AuthenticationReducer,
    form: formReducer,
};

// This type can be used as a hint on action creators so that its 'dispatch' and 'getState' params are
// correctly typed to match your store.
export type IAppThunkAction<TAction> =
    (dispatch: (action: TAction) => void, getState: () => IApplicationState) => void;
