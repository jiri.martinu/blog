import { History } from 'history';
import { routerMiddleware, connectRouter } from 'connected-react-router'
import { applyMiddleware, combineReducers, compose, createStore, StoreEnhancer, ReducersMapObject, Store, StoreEnhancerStoreCreator } from 'redux';
import thunk from 'redux-thunk';
import { IApplicationState, reducers } from '.';

export default function configureStore(history: History, initialState?: IApplicationState) {
    // Build middleware. These are functions that can process the actions before they reach the store.
    const windowIfDefined = typeof window === 'undefined' ? null : window as any;
    // If devTools is installed, connect to it
    const devToolsExtension = windowIfDefined && windowIfDefined.__REDUX_DEVTOOLS_EXTENSION__ as () => StoreEnhancer;
    const createStoreWithMiddleware = compose(
        applyMiddleware(thunk, routerMiddleware(history)),
        devToolsExtension ? devToolsExtension() : <S>(next: StoreEnhancerStoreCreator<S>) => next
    )(createStore);

    // Combine all reducers and instantiate the app-wide store instance
    const allReducers = buildRootReducer(reducers, history);
    const store = createStoreWithMiddleware(allReducers, initialState) as Store<IApplicationState>;

    return store;
}

function buildRootReducer(allReducers: ReducersMapObject, history: History) {
    return combineReducers<IApplicationState>(Object.assign({}, allReducers, { router: connectRouter(history) } as any));
}
