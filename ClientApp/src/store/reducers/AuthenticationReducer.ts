import { UsersConstants } from '../constants/UsersConstants';
import { Action, Reducer } from 'redux';
import { IAuthenticationState } from '../states/AuthenticationState';
import { KnownLoginAction, KnownLogoutAction } from '../actions/AuthenticationActions';

const user = JSON.parse(localStorage.getItem('user'));
const unloadedState = user ? { loggedIn: true, user } : { loggedIn: false, user: null };

export const AuthenticationReducer: Reducer<IAuthenticationState> = (state: IAuthenticationState = unloadedState, incomingAction: Action) => {
    const action = incomingAction as KnownLoginAction | KnownLogoutAction;

    switch (action.type) {
        case UsersConstants.LOGIN_REQUEST:
            return {
                loggedIn: false,
                user: action.user
            };
        case UsersConstants.LOGIN_SUCCESS:
            return {
                loggedIn: true,
                user: action.user
            };
        case UsersConstants.LOGIN_FAILURE:
            return {
                ...state
            };
        case UsersConstants.LOGOUT_REQUEST:
            return {
                ...state
            };
        case UsersConstants.LOGOUT_SUCCESS:
            return {
                loggedIn: false,
                user: null,
            };
        case UsersConstants.LOGOUT_FAILURE:
            return {
                ...state
            };                          
        default:
            const exhaustiveCheck: never = action;
    }

    return state || unloadedState;
}