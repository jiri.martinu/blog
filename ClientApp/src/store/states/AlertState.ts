// -----------------
// STATE - This defines the type of data maintained in the Redux store.

export interface IAlertState {
    type: string;
    message: string;
}
