import { IAppThunkAction } from '..';
import { IUser } from '../states/AuthenticationState';
import { UsersConstants } from '../constants/UsersConstants';
import { actionCreators as AlertActionCreators, KnownAlertAction } from './AlertActions';
import { userService as UserService } from '../../services/UsersService';
import { push, RouterAction } from 'connected-react-router';

interface IUsersLoginRequest {
    type: UsersConstants.LOGIN_REQUEST;
    user: IUser;
}

interface IUsersLoginSuccess {
    type: UsersConstants.LOGIN_SUCCESS;
    user: IUser;
}

interface IUsersLoginFailure {
    type: UsersConstants.LOGIN_FAILURE;
    error: any;
}

export type KnownLoginAction = IUsersLoginRequest | IUsersLoginSuccess | IUsersLoginFailure;

const login = (username: string, password:string): IAppThunkAction<KnownLoginAction | KnownAlertAction | RouterAction> => async (dispatch, getState) => {
    const request = (user: IUser): IUsersLoginRequest => ({ type: UsersConstants.LOGIN_REQUEST, user })
    const success = (user: IUser): IUsersLoginSuccess => ({ type: UsersConstants.LOGIN_SUCCESS, user })
    const failure = (error: any): IUsersLoginFailure => ({ type: UsersConstants.LOGIN_FAILURE, error })

    // Setting name will allow us show user name in error.
    dispatch(request({ username }));

    await UserService.login(username, password)
        .then(
            data => {
                dispatch(success(data));
                dispatch(push("/"));
                dispatch(AlertActionCreators.success('Login successful'));
            },
            error => {
                dispatch(failure(error));
                dispatch(AlertActionCreators.error(error));
            }
        );
}

interface IUsersLogoutRequest {
    type: UsersConstants.LOGOUT_REQUEST;
}

interface IUsersLogoutSuccess {
    type: UsersConstants.LOGOUT_SUCCESS;
}

interface IUsersLogoutFailure {
    type: UsersConstants.LOGOUT_FAILURE;
    error: any;
}

export type KnownLogoutAction = IUsersLogoutRequest | IUsersLogoutSuccess | IUsersLogoutFailure;

const logout = (): IAppThunkAction<KnownLogoutAction | KnownAlertAction | RouterAction> => async (dispatch, getState) => {
    const request = (): IUsersLogoutRequest => ({ type: UsersConstants.LOGOUT_REQUEST })
    const success = (): IUsersLogoutSuccess => ({ type: UsersConstants.LOGOUT_SUCCESS })
    const failure = (error: any): IUsersLogoutFailure => ({ type: UsersConstants.LOGOUT_FAILURE, error })

    dispatch(request());

    await UserService.logout()
        .then(
            _ => { 
                dispatch(success());
                dispatch(push("/"));
                dispatch(AlertActionCreators.success('Logout successful'));
            },
            error => {
                dispatch(failure(error));
                dispatch(AlertActionCreators.error(error));
            }
        );
}

interface IUsersRegisterRequest {
    type: UsersConstants.REGISTER_REQUEST;
}

interface IUsersRegisterSuccess {
    type: UsersConstants.REGISTER_SUCCESS;
}

interface IUsersRegisterFailure {
    type: UsersConstants.REGISTER_FAILURE;
    error: any;
}

export type KnownRegisterAction = IUsersRegisterRequest | IUsersRegisterSuccess | IUsersRegisterFailure;

const register = (user: IUser): IAppThunkAction<KnownRegisterAction | KnownAlertAction | RouterAction> => async (dispatch, getState) => {
    const request = (): IUsersRegisterRequest => ({ type: UsersConstants.REGISTER_REQUEST })
    const success = (): IUsersRegisterSuccess => ({ type: UsersConstants.REGISTER_SUCCESS })
    const failure = (error): IUsersRegisterFailure => ({ type: UsersConstants.REGISTER_FAILURE, error })

    dispatch(request());

    await UserService.register(user)
        .then(
            () => { 
                dispatch(success());
                dispatch(push("/"));
                dispatch(AlertActionCreators.success('Registration successful'));
            },
            error => {
                dispatch(failure(error));
                dispatch(AlertActionCreators.error(error));
            }
        );
}

interface IUsersGetAntiforgeryTokenRequest {
    type: UsersConstants.GETANTIFORGERYTOKEN_REQUEST;
}

interface IUsersGetAntiforgeryTokenSuccess {
    type: UsersConstants.GETANTIFORGERYTOKEN_SUCCESS;
}

interface IUsersGetAntiforgeryTokenFailure {
    type: UsersConstants.GETANTIFORGERYTOKEN_FAILURE;
    error: any;
}

export type KnownGetAntiforgeryTokenAction = IUsersGetAntiforgeryTokenRequest | IUsersGetAntiforgeryTokenSuccess | IUsersGetAntiforgeryTokenFailure;

const getAntiForgeryToken = (): IAppThunkAction<KnownGetAntiforgeryTokenAction> => async (dispatch, getState) => {
    const request = (): IUsersGetAntiforgeryTokenRequest => ({ type: UsersConstants.GETANTIFORGERYTOKEN_REQUEST })
    const success = (): IUsersGetAntiforgeryTokenSuccess => ({ type: UsersConstants.GETANTIFORGERYTOKEN_SUCCESS })
    const failure = (error: any): IUsersGetAntiforgeryTokenFailure => ({ type: UsersConstants.GETANTIFORGERYTOKEN_FAILURE, error })

    dispatch(request());

    await UserService.getAntiForgeryToken()
        .then(
            _ => { 
                dispatch(success());
            },
            error => {
                dispatch(failure(error));
            }
        );
}

/* interface IUsersGetAllRequest {
    type: UsersConstants.GETALL_REQUEST;
}

interface IUsersGetAllSuccess {
    type: UsersConstants.GETALL_SUCCESS;
    users: IUser[]
}

interface IUsersGetAllFailure {
    type: UsersConstants.GETALL_FAILURE;
    error: any;
}

export type KnownGetAllAction = IUsersGetAllRequest | IUsersGetAllSuccess | IUsersGetAllFailure;

const getAll = (): IAppThunkAction<KnownGetAllAction> => async (dispatch, getState) => {
    const request = (): IUsersGetAllRequest => ({ type: UsersConstants.GETALL_REQUEST })
    const success = (users: IUser[]): IUsersGetAllSuccess => ({ type: UsersConstants.GETALL_SUCCESS, users })
    const failure = (error): IUsersGetAllFailure => ({ type: UsersConstants.GETALL_FAILURE, error })

    dispatch(request());

    UserService.getAll()
        .then(
            (users: IUser[]) => dispatch(success(users)),
            error => dispatch(failure(error))
        );
}

interface IUsersRemoveRequest {
    type: UsersConstants.REMOVE_REQUEST;
    id: number;
}

interface IUsersRemoveSuccess {
    type: UsersConstants.REMOVE_SUCCESS;
    id: number;
}

interface IUsersRemoveFailure {
    type: UsersConstants.REMOVE_FAILURE;
    id: number;
    error: any;
}

export type KnownRemoveAction = IUsersRemoveRequest | IUsersRemoveSuccess | IUsersRemoveFailure;

const remove = (id: number): IAppThunkAction<KnownRemoveAction> => async (dispatch, getState) => {
    const request = (id: number): IUsersRemoveRequest => ({ type: UsersConstants.REMOVE_REQUEST, id })
    const success = (id: number): IUsersRemoveSuccess => ({ type: UsersConstants.REMOVE_SUCCESS, id })
    const failure = (id: number, error): IUsersRemoveFailure => ({ type: UsersConstants.REMOVE_FAILURE, id, error })

    dispatch(request(id));

    UserService.remove(id)
        .then(
            () => { 
                dispatch(success(id));
            },
            error => {
                dispatch(failure(id, error));
            }
        );
} */

export const actionCreators = {
    login,
    logout,
    register,
    getAntiForgeryToken,
/*     getAll,
    remove */
};
