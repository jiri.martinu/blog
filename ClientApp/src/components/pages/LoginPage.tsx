import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { actionCreators as UsersActionsCreators } from '../../store/actions/AuthenticationActions';
import { IAuthenticationState } from 'src/store/states/AuthenticationState';
import LoginForm, { ILoginFormData } from '../forms/LoginForm';
import { IApplicationState } from 'src/store';

type LoginPageProps = 
    IAuthenticationState
    & typeof UsersActionsCreators
    & RouteComponentProps<{}>;

class LoginPage extends React.Component<LoginPageProps, {}> {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    public async handleSubmit(user: ILoginFormData) {
        await this.props.login(user.username, user.password);
    }

    public render() {
        return <div className="col-md-6 col-md-offset-3">
            <h2>Login</h2>
            <LoginForm onSubmit={this.handleSubmit}/>
        </div>
    }
}

export default connect(
    (state: IApplicationState) => state.authentication,
    UsersActionsCreators
)(LoginPage);