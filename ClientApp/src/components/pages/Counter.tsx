import * as React from 'react';
import { connect } from 'react-redux';
import { IApplicationState }  from '../../store';
import * as CounterStore from '../../store/Counter';
import { DefaultButton } from '../atoms/buttons/ButtonsStyles';

type CounterProps =
    CounterStore.ICounterState
    & typeof CounterStore.actionCreators;

class Counter extends React.Component<CounterProps, {}> {
    public constructor(props: Readonly<CounterProps>) {
        super(props);

        this.onClickHandle = this.onClickHandle.bind(this);
    }

    public onClickHandle() {
        this.props.increment();
    }

    public render() {
        return <div>
            <h1>Counter</h1>

            <p>This is a simple example of a React component.</p>

            <p>Current count: <strong>{ this.props.count }</strong></p>

            <DefaultButton onClick={ this.onClickHandle } className='btn' isFilled={false}>
                Increment
            </DefaultButton>
        </div>;
    }
}

// Wire up the React component to the Redux store
export default connect(
    (state: IApplicationState) => state.counter, // Selects which state properties are merged into the component's props
    CounterStore.actionCreators                 // Selects which action creators are merged into the component's props
)(Counter);