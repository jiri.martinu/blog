import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { actionCreators as UsersActionsCreators } from '../../store/actions/AuthenticationActions';
import { IApplicationState } from 'src/store';
import RegistrationForm, { IRegistrationFormData } from '../forms/RegistrationForm';

type RegisterPageProps = 
    typeof UsersActionsCreators
    & RouteComponentProps<{}>;

class RegistrationPage extends React.Component<RegisterPageProps, {}> {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    public async handleSubmit(user: IRegistrationFormData) {
        await this.props.register(user);
    }

    public render() {
        return <div className="col-md-6 col-md-offset-3">
            <h2>Register</h2>
            <RegistrationForm onSubmit={this.handleSubmit} />
        </div>
    }
}

export default connect(
    (state: IApplicationState) => {
        return {}
    },
    UsersActionsCreators
)(RegistrationPage);