import * as React from 'react';
import { RouteComponentProps, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { actionCreators as UsersActionsCreators } from '../../store/actions/AuthenticationActions';
import { IApplicationState } from 'src/store';

type LogoutPageProps = 
    typeof UsersActionsCreators
    & RouteComponentProps<{}>; 

class LogoutPage extends React.Component<LogoutPageProps, {}> {
    public componentWillMount() {
        this.props.logout();
    }

    public render() {
        return null;
    }
}

export default connect(
    (state: IApplicationState) => {
        return {}
    },
    UsersActionsCreators                // Selects which action creators are merged into the component's props
)(LogoutPage);