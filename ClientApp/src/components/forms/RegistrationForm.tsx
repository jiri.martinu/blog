import * as React from 'react';
import { InjectedFormProps, reduxForm, Field } from 'redux-form'
import { Link } from 'react-router-dom';
import RenderField from 'src/utils/forms/RenderField';
import { Required } from '../../utils/forms/FormFieldValidations';

export interface IRegistrationFormData {
    username: string;
    password: string;
}

class RegistrationForm extends React.Component<InjectedFormProps<IRegistrationFormData>, {}> {
    public render() {
        const { pristine, submitting, reset, handleSubmit } = this.props;

        return <form name="form" onSubmit={handleSubmit}>
            <Field
                name="username"
                type="text"
                component={RenderField}
                label="Username"
                validate={[Required]}
            />
            <Field
                name="password"
                type="password"
                component={RenderField}
                label="Password"
                validate={[Required]}
            />
            <div className="form-group">
                <button className="btn btn-primary" type="submit" disabled={submitting}>
                    Register
                </button>
                <button className="btn btn-default" type="button" disabled={pristine || submitting} onClick={reset}>
                    Clear Values
                </button>
                
                {submitting && 
                    <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                }
                <Link to="/login" className="btn btn-link">Cancel</Link>
            </div>
        </form>;
    }
}

export default reduxForm<IRegistrationFormData>({
    form: 'registrationForm',
})(RegistrationForm);