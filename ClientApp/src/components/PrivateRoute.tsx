import * as React from 'react';
import { Route, Redirect, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { IApplicationState } from 'src/store';
import { IAuthenticationState } from 'src/store/states/AuthenticationState';

interface IPrivateRouteProps {
    path: string,
    router: RouteComponentProps<{}>,
    authentication: IAuthenticationState,
    component: any
};

class PrivateRoute extends React.Component<IPrivateRouteProps, {}> {
    constructor(props) {
        super(props);

        this.resolveComponent = this.resolveComponent.bind(this);
    }

    public resolveComponent() {
        const { component: Component } = this.props;

        return this.props.authentication.loggedIn
            ? <Component {...this.props} />
            : <Redirect to={{ pathname: '/login', state: { from: this.props.router.location } }} />
    }

    public render() {
        const { component: Component, ...rest } = this.props;
        
        return <Route {...rest} render={this.resolveComponent} />
    }
}

export default connect(
    (state: IApplicationState) => {
        return {
            router: (state as any).router,
            authentication: state.authentication
        }
    }
)(PrivateRoute);