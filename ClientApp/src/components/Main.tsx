import * as React from 'react';
import { Switch, Route, RouteComponentProps, withRouter } from 'react-router-dom';
import Home from './pages/Home';
import FetchData from './pages/FetchData';
import Counter from './pages/Counter';
import { IApplicationState } from 'src/store';
import { connect } from 'react-redux';
import { IAlertState } from 'src/store/states/AlertState';
import RegistrationPage from './pages/RegistrationPage';
import LoginPage from './pages/LoginPage';
import LogoutPage from './pages/LogoutPage';
import PrivateRoute from 'src/components/PrivateRoute';
import { Grid } from 'react-bootstrap';
import { compose } from 'redux';
import { actionCreators as AlertActionsCreators } from '../store/actions/AlertActions';
import { actionCreators as UsersActionsCreators } from '../store/actions/AuthenticationActions';

// At runtime, Redux will merge together...
type MainProps = IAlertState &
    typeof AlertActionsCreators &
    typeof UsersActionsCreators &
    RouteComponentProps<{}>;

class Main extends React.Component<MainProps, {}> {
    constructor(props) {
        super(props);

        this.props.history.listen((location, action) => {
            if (this.props.type !== null) {
                this.props.clear();
            }
        }).bind(this);

        // In Production this is not needed but when you use proxy for frontend in Development then you have do this.  
        this.props.getAntiForgeryToken();
    }
    
    public render() {
        return <main>
            {this.props.message &&
                <div className={`alert ${this.props.type}`}>{this.props.message}</div>
            }
            <Grid>
                <Switch>
                    <Route exact={true} path='/' component={Home} />
                    <PrivateRoute path='/counter' component={Counter} />
                    <Route path='/fetchdata/:startDateIndex?' component={FetchData} />
                    <Route path='/registration' component={ RegistrationPage } />
                    <Route path='/login' component={ LoginPage } />
                    <Route path='/logout' component={ LogoutPage } />
                </Switch>
            </Grid>
        </main>
    }
}

export default compose(
    withRouter,
    connect(
        (state: IApplicationState) => state.alert,
        {
            ...AlertActionsCreators,
            ...UsersActionsCreators,
        }
    )
)(Main);
