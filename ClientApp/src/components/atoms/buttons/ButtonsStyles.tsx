import styled, { css, ThemeProps, ThemedStyledFunction } from 'styled-components';
import { Link } from 'react-router-dom';
import * as React from 'react';
import { darken } from 'polished';
import { Button } from 'react-bootstrap';

interface IDefaultLinkButtonProps extends ThemeProps<any> {
    isFilled: boolean;
}

export const DefaultLinkButtonStyle = css<IDefaultLinkButtonProps>`
    background-color: ${props => props.isFilled ? props.theme.colors.fonts.links : 'transparent'};
    color: ${props => props.isFilled ? '#fff' : props.theme.colors.fonts.links};
    border: 1px solid ${props => props.theme.colors.fonts.links};
    border-radius: ${props => props.theme.borders.radius};
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in--out, box-shadow 0.15s ease-in-out;

    &:hover {
        color: #fff;
        background-color: ${props => props.isFilled ? darken('0.1', props.theme.colors.fonts.links) : props.theme.colors.fonts.links};
        border-color: ${props => props.theme.colors.fonts.links}; 
    }

    &:focus {
        color: #fff;
        box-shadow: 0 0 0 0.2rem rgba(25, 151, 198, 0.5); 
    }
`

export const LinkButton = styled(({ isFilled, ...rest }) => <Link {...rest} />)`
    &&&& {
        ${DefaultLinkButtonStyle}
    }
`

export const DefaultButton = styled(({ isFilled, ...rest }) => <Button {...rest} />)`
    &&&& {
        ${DefaultLinkButtonStyle}

        text-shadow: none;
    }
`

export const MenuButton = styled(({ isFilled, ...rest }) => <Link {...rest} />)`
    &&&& {
        ${DefaultLinkButtonStyle}

        padding: 10px;
    }
`