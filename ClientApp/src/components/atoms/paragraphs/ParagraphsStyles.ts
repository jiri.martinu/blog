import styled from "styled-components";

export const Perex = styled.p`
    font-size: 2.1rem;
`

export const Paragraph = styled.p`
    font-size: 1.6rem;
`