import * as React from "react";
import styled from "styled-components";

interface INavWrapProps {
    active?: any;
    activeKey?: any; 
    activeHref?: any;
    onSelect?: any;
    children?: any;
}

class NavItemWrapper extends React.Component<INavWrapProps, {}> {
    public render() {
        const {
            active,
            activeKey,
            activeHref,
            onSelect,
            children,
            ...otherProps
        } = this.props;

        return <li role="presentation" className={(this.props as any).className} {...otherProps}>
            {children}
        </li>
    }
}

export const StyledNavItemWrapper = styled(NavItemWrapper)`
    &&&& {
        min-height: 50px;
        display: flex;
        align-items: center;
    }
`


export default NavItemWrapper;
