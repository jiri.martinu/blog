import * as React from 'react';
import { IApplicationState } from 'src/store';
import { connect } from 'react-redux';
import { IAuthenticationState } from 'src/store/states/AuthenticationState';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { StyledNavMenu } from '../nav/NavMenuStyles';

/* interface IHeaderProps {
    router: RouterState,
    authentication: IAuthenticationState
}; */

type HeaderProps = 
    IAuthenticationState
    & RouteComponentProps<{}>;

class Header extends React.Component<HeaderProps, {}> {
    public render() {
        return <header>
            <StyledNavMenu />
        </header>
    }
}

export default compose(
    withRouter,
    connect(
        (state: IApplicationState) => state.authentication,
    )
)(Header);
