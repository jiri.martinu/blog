import * as React from 'react';
import { Glyphicon, Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { IApplicationState } from 'src/store';
import { connect } from 'react-redux';
import { IAuthenticationState } from 'src/store/states/AuthenticationState';
import { compose } from 'redux';
import { StyledNavItemWrapper } from 'src/components/atoms/links/NavItemWrapper';
import { MenuButton } from 'src/components/atoms/buttons/ButtonsStyles';

type NavMenuProps = 
    IAuthenticationState &
    RouteComponentProps<{}>;

class NavMenu extends React.Component<NavMenuProps, {}> {
    public render() {
        return <Navbar fixedTop={true} fluid={false} collapseOnSelect={true} className={(this.props as any).className}>
            <Navbar.Header>
                <Navbar.Brand>
                    <Link to={'/'}>Blog</Link>
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                <Nav>
                    <LinkContainer to={'/'} exact={true}>
                        <NavItem>
                            <Glyphicon glyph='home' /> Home
                        </NavItem>
                    </LinkContainer>
                    <LinkContainer to={'/counter'}>
                        <NavItem>
                            <Glyphicon glyph='education' /> Counter
                        </NavItem>
                    </LinkContainer>
                    <LinkContainer to={'/fetchdata'}>
                        <NavItem>
                            <Glyphicon glyph='th-list' /> Fetch data
                        </NavItem>
                    </LinkContainer>
                </Nav>
                <Nav pullRight={true}>
                    {this.props.loggedIn &&
                        <LinkContainer to={'/logout'}>
                            <NavItem>
                                <Glyphicon glyph='log-out' /> Logout
                            </NavItem>
                        </LinkContainer>
                    }
                    {!this.props.loggedIn &&
                        <StyledNavItemWrapper>
                            <MenuButton to={'/login'} className='btn' isFilled={true}>
                                <Glyphicon glyph='user' /> Login
                            </MenuButton>
                        </StyledNavItemWrapper>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    }
}

export default compose(
    withRouter,
    connect(
        (state: IApplicationState) => state.authentication
    )
)(NavMenu);
