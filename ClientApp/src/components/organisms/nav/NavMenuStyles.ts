import styled from 'styled-components';
import NavMenu from './NavMenu';

export const StyledNavMenu = styled(NavMenu)`
    background-color: ${props => props.theme.colors.backgrounds.menu} !important;
    background-image: none;
    border: none;

    .navbar-brand, .navbar-nav > li > a {
        text-shadow: none;
        color: rgba(255, 255, 255, 0.5);

        &:hover, &:focus {
            color: rgba(255, 255, 255, 0.75);
        }
    }

    .navbar-nav {
        & > .active { 
            & > a {
                color: white;
                font-weight: 500;
                background-image: none;
                background-color: transparent;

                &:hover, &:focus {
                    color: white;
                    background-color: transparent;
                }
            }
        }
        & .open {
            & > a {
                background-image: none;
            }
        }
    }
`;