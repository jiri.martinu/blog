import 'bootstrap/dist/css/bootstrap-theme.css';
import 'bootstrap/dist/css/bootstrap.css';
import { createBrowserHistory } from 'history';
import configureStore from './store/configureStore';
import { IApplicationState } from './store';
import { Provider } from 'react-redux';
import * as React from 'react';
import registerServiceWorker from './registerServiceWorker';
import App from './App';
import { ConnectedRouter } from 'connected-react-router';
import { render } from 'react-snapshot';
import WebFont from 'webfontloader';

// Create browser history to use in the Redux store
const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href');
const history = createBrowserHistory({ basename: baseUrl != null ? baseUrl : "" });

// Get the application-wide store instance, prepopulating with state from the server where available.
const initialState = (window as any).initialReduxState as IApplicationState;

const store = configureStore(history, initialState);

const rootElement = document.getElementById('root');

WebFont.load({
  google: {
    families: ['Roboto:300,400,500,700,400italic', 'sans-serif']
  }
});

render(
  <Provider store={store}>
    <ConnectedRouter history={history as any}>
      <App />
    </ConnectedRouter>
  </Provider>,
  rootElement
);

registerServiceWorker();
