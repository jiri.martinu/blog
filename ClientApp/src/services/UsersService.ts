import { config } from '../utils/config';
import { IUser } from 'src/store/states/AuthenticationState';
import { FetchHeader } from 'src/utils/FetchHeader';

const login = (username: string, password: string): Promise<{}> => {
    return fetch(config.apiUrl + '/api/account/login', FetchHeader(JSON.stringify({ username, password })))
        .then(handleResponse, handleError)
        .then((user) => {
            if (user) {
                localStorage.setItem('user', JSON.stringify(user));
            }

            return user;
        });
}

const logout = () => {
    return fetch(config.apiUrl + '/api/account/logout', FetchHeader())
        .then(handleResponse, handleError)
        .then(() => localStorage.removeItem('user'));
}

const register = (user: IUser): Promise<{}> => {
    return fetch(config.apiUrl + '/api/account/register', FetchHeader(JSON.stringify(user)))
        .then(handleResponse, handleError);
}

const getAntiForgeryToken = (): Promise<{}> => {
    return fetch(config.apiUrl + '/api/antiforgery')
        .then(handleResponse, handleError);
}

/* const getAll = () => {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(config.apiUrl + '/users', requestOptions).then(handleResponse, handleError);
}

const getById = (id: number) => {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(config.apiUrl + '/users/' + id, requestOptions).then(handleResponse, handleError);
}

const update = (user: IUser) => {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(config.apiUrl + '/users/' + user.id, requestOptions).then(handleResponse, handleError);
}

const remove = (id: number) => {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(config.apiUrl + '/users/' + id, requestOptions).then(handleResponse, handleError);
} */

const handleResponse = (response) => {
    return new Promise((resolve, reject) => {
        if (response.ok) {
            // return json if it was returned in the response
            const contentType = response.headers.get("content-type");

            if (contentType && contentType.includes("application/json")) {
                response.json().then(json => resolve(json));
            } else {
                resolve();
            }
        } else {
            // return error message from response body
            response.text().then(text => reject(text));
        }
    });
}

const handleError = (error) => {
    return Promise.reject(error && error.message);
}

export const userService = {
    login,
    logout,
    register,
    getAntiForgeryToken,
/*     getAll,
    getById,
    update,
    remove  */
};