import * as React from "react";
import { WrappedFieldProps } from "redux-form";
import styled, { ThemeProps } from "styled-components";

interface IExtendedWrappedFieldProps extends WrappedFieldProps {
    name: string;
    label: string;
    type: string;
}

export default class RenderField extends React.Component<IExtendedWrappedFieldProps, {}> {
    public render() {
        const { name, input, label, type, meta: { touched, error, warning } } = this.props;

        return <div className={'form-group' + (error ? ' has-error' : '')}>
            {/* <label htmlFor={name}>{label}</label> */}
            <div>
                <input {...input} placeholder={label} type={type} />
                {touched &&
                    ((error && <StyledFormErrorOrWarning isError={true}>{error}</StyledFormErrorOrWarning>) ||
                        (warning && <StyledFormErrorOrWarning>{warning}</StyledFormErrorOrWarning>))}
            </div>
        </div>
    }
}

interface IStyledFormErrorOrWarningProps extends ThemeProps<any> {
    isError?: boolean;
}

const StyledFormErrorOrWarning = styled.span<IStyledFormErrorOrWarningProps>`
    &&&& {
        margin-left: 0.25rem;
        color: ${props => props.isError ? props.theme.colors.fonts.danger : props.theme.colors.fonts.warning}
    }
`