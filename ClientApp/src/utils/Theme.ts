import { lighten, darken } from "polished";

export const Theme = {
  colors: {
    backgrounds: {
      background: "#252830",
      menu: "#1a1c22",
      input: "#434857"
    },
    fonts: {
      default: "#cfd2da",
      titles: "white",
      links: "#1997c6",
      inputs: lighten("0.4", "#434857"),
      danger: "#e64759",
      danger_hover: darken("0.4", "#e64759"),
      warging: "#e4d836",
      warging_hover: darken("0.4", "#e4d836"),
    },
    borders: {
      table: "#434857",
    }
  },
  borders: {
    radius: "0.25rem",
  }
};