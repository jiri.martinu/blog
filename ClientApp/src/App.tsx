import * as React from 'react';
import { hot } from 'react-hot-loader';
import Main from './components/Main';
import Header from './components/organisms/header/Header';
import { GlobalStyles } from './globalStyles';
import { ThemeProvider } from 'styled-components';
import { Theme } from './utils/Theme';

class App extends React.Component<{}, {}> {
    public render() {
        return <ThemeProvider theme={Theme}>
            <>
                <GlobalStyles />
                <Header />
                <Main />
            </>
        </ThemeProvider>
    }
}

export default hot(module)(App);
