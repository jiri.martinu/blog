const { injectBabelPlugin } = require('react-app-rewired');
const rewireStyledComponents = require('react-app-rewire-styled-components');

function rewireStyledComponentsCssNamespace(
  config,
  env,
  styledComponentsPluginOptions = {}
) {
  return injectBabelPlugin(
    ['@quickbaseoss/babel-plugin-styled-components-css-namespace', styledComponentsPluginOptions],
    config
  );
}

module.exports = function override(config, env) {
    config = rewireStyledComponents(config, env);
    config = rewireStyledComponentsCssNamespace(config, env);

    return config;
}
