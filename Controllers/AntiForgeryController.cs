using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Mvc;

namespace Blog.Controllers
{
    [ApiController]
    public class AntiForgeryController : Controller
    {
        [Route("api/antiforgery")]
        [IgnoreAntiforgeryToken]
        public IActionResult Index()
        {
            return NoContent();
        }
    }
}