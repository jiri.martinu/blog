using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Blog.Models.Dtos;
using Blog.Models;
using AutoMapper;

namespace Blog.Controllers
{
    [ApiController, Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<User> UserManager;

        private readonly SignInManager<User> SignInManager;
        private readonly IMapper Mapper;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, IMapper mapper)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            Mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]UserLoginRegisterDTO registerRequest)
        {
            var newUser = new User
            {
                UserName = registerRequest.Username
            };

            var creationResult = await UserManager.CreateAsync(newUser, registerRequest.Password);

            if (!creationResult.Succeeded) {
                return BadRequest(creationResult.Errors.Select(error => error.Description).Aggregate((errorDescriptions, identityError) => errorDescriptions + $", {identityError}"));
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody]UserLoginRegisterDTO loginRequest)
        {
            var user = await UserManager.FindByNameAsync(loginRequest.Username);

            if (user == null) {
                return BadRequest("Login failed");
            }

            var signInResult = await SignInManager.PasswordSignInAsync(user, loginRequest.Password, isPersistent: true, lockoutOnFailure: false); //lockout on failure is off because this is a demo

            if (!signInResult.Succeeded) {
                return BadRequest("Login failed");
            }

            return Ok(Mapper.Map<UserFrontDTO>(user));
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await SignInManager.SignOutAsync();

            return NoContent();
        }
    }
}