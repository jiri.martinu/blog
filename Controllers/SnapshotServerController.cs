using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Blog.Controllers
{
    public class SnapshotServerController : Controller
    {
        [IgnoreAntiforgeryToken]
        public IActionResult Serve()
        {
            string fullPathName = CreateFullPath(Request.Path.Value);

            if (!System.IO.File.Exists(fullPathName)) {
                string indexFullPathName = CreateFullPath("/index");

                if (!System.IO.File.Exists(indexFullPathName)) {
                    throw new Exception("Something is not working properly. Probably you!");
                } else {
                    fullPathName = indexFullPathName;
                }
            }

            return new ContentResult {
                ContentType = "text/html",
                StatusCode = (int)HttpStatusCode.OK,
                Content = System.IO.File.ReadAllText(fullPathName)
            };
        }

        private string CreateFullPath(string fileName) 
        {
            return "ClientApp/build" + fileName + ".html";
        }
    }
}
