using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Blog.ExtensionMethods
{
    public static class MyExtensions
    {
        public static string ToHumanReadable(this decimal number)
        {
            return number.ToString("f99").TrimEnd('0');
        }

        public static decimal ToDecimal(this string number)
        {
            CultureInfo cultureInfo = number.Contains(",") ? CultureInfo.CurrentCulture : CultureInfo.InvariantCulture;

            return decimal.Parse(number, cultureInfo);
        }

        public static string GetTimestamp(this DateTime value) {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        public static IEnumerable<T> GetEnumValues<T>() {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
    }   
}